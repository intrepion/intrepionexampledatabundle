<?php

namespace Intrepion\Example\DataBundle\DataFixtures\ORM;

use Intrepion\Example\DataBundle\Entity\SampleB;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class SampleBFixtures extends AbstractFixture implements OrderedFixtureInterface
{
    public function getOrder()
    {
        return 1;
    }

    public function load(ObjectManager $manager)
    {
        $count = 0;
        $referenceName = 'sample-b-';

        $object = new SampleB();
        $object->setSampleName('Sophia');
        $object->setSampleBoolean(false);
        $object->setSampleInteger(1235);
        $object->setSampleString('Abcd, Efg!');
        $object->setSampleText('one thousand two hundred thirty-four dollars and fifty-six cents');
        $object->setSampleDatetime(new \DateTime('2013-12-11T10:09:08'));
        $object->setSampleDate(new \DateTime('2012-11-10'));
        $object->setSampleTime(new \DateTime('11:10:09'));
        $object->setSampleDecimal(1234.56);
        $manager->persist($object);
        $manager->flush($object);
        $this->addReference($referenceName . $count, $object);
        $count++;

        $object = new SampleB();
        $object->setSampleName('Aiden');
        $object->setSampleBoolean(false);
        $object->setSampleInteger(7890);
        $object->setSampleString('Hijk, Lmn!');
        $object->setSampleText('seven thousand eight hundred ninety dollars and thirteen cents');
        $object->setSampleDatetime(new \DateTime('2013-12-11T10:09:09'));
        $object->setSampleDate(new \DateTime('2012-11-09'));
        $object->setSampleTime(new \DateTime('10:09:08'));
        $object->setSampleDecimal(7890.13);
        $manager->persist($object);
        $manager->flush($object);
        $this->addReference($referenceName . $count, $object);
        $count++;

        $object = new SampleB();
        $object->setSampleName('Emma');
        $object->setSampleBoolean(false);
        $object->setSampleInteger(5791);
        $object->setSampleString('Opqr, Stu!');
        $object->setSampleText('five thousand seven hundred ninety-one dollars and forty-seven cents');
        $object->setSampleDatetime(new \DateTime('2013-12-11T10:09:10'));
        $object->setSampleDate(new \DateTime('2012-11-08'));
        $object->setSampleTime(new \DateTime('09:08:07'));
        $object->setSampleDecimal(5791.47);
        $manager->persist($object);
        $manager->flush($object);
        $this->addReference($referenceName . $count, $object);
        $count++;

        $object = new SampleB();
        $object->setSampleName('Jackson');
        $object->setSampleBoolean(false);
        $object->setSampleInteger(483);
        $object->setSampleString('Vwxy, Zab!');
        $object->setSampleText('four hundred eighty-two dollars and seventy-two cents');
        $object->setSampleDatetime(new \DateTime('2013-12-11T10:09:11'));
        $object->setSampleDate(new \DateTime('2012-11-07'));
        $object->setSampleTime(new \DateTime('08:07:06'));
        $object->setSampleDecimal(482.72);
        $manager->persist($object);
        $manager->flush($object);
        $this->addReference($referenceName . $count, $object);
        $count++;

        $object = new SampleB();
        $object->setSampleName('Olivia');
        $object->setSampleBoolean(false);
        $object->setSampleInteger(8420);
        $object->setSampleString('Cdef, Ghi!');
        $object->setSampleText('eight thousand four hundred nineteen dollars and eighty-eight cents');
        $object->setSampleDatetime(new \DateTime('2013-12-11T10:09:12'));
        $object->setSampleDate(new \DateTime('2012-11-06'));
        $object->setSampleTime(new \DateTime('07:06:05'));
        $object->setSampleDecimal(8419.88);
        $manager->persist($object);
        $manager->flush($object);
        $this->addReference($referenceName . $count, $object);
        $count++;

        $object = new SampleB();
        $object->setSampleName('Ethan');
        $object->setSampleBoolean(true);
        $object->setSampleInteger(9025);
        $object->setSampleString('Jklm, Nop!');
        $object->setSampleText('nine thousand twenty-four dollars and sixty-eight cents');
        $object->setSampleDatetime(new \DateTime('2013-12-11T10:09:13'));
        $object->setSampleDate(new \DateTime('2012-11-05'));
        $object->setSampleTime(new \DateTime('06:05:04'));
        $object->setSampleDecimal(9024.68);
        $manager->persist($object);
        $manager->flush($object);
        $this->addReference($referenceName . $count, $object);
        $count++;

        $object = new SampleB();
        $object->setSampleName('Isabella');
        $object->setSampleBoolean(false);
        $object->setSampleInteger(369);
        $object->setSampleString('Qrst, Uvw!');
        $object->setSampleText('three hundred sixty-nine dollars and twenty-six cents');
        $object->setSampleDatetime(new \DateTime('2013-12-11T10:09:14'));
        $object->setSampleDate(new \DateTime('2012-11-04'));
        $object->setSampleTime(new \DateTime('05:04:03'));
        $object->setSampleDecimal(369.26);
        $manager->persist($object);
        $manager->flush($object);
        $this->addReference($referenceName . $count, $object);
        $count++;

        $object = new SampleB();
        $object->setSampleName('Liam');
        $object->setSampleBoolean(true);
        $object->setSampleInteger(506);
        $object->setSampleString('Xyza, Bcd!');
        $object->setSampleText('five hundred six dollars and twenty-nine cents');
        $object->setSampleDatetime(new \DateTime('2013-12-11T10:09:15'));
        $object->setSampleDate(new \DateTime('2012-11-03'));
        $object->setSampleTime(new \DateTime('04:03:02'));
        $object->setSampleDecimal(506.29);
        $manager->persist($object);
        $manager->flush($object);
        $this->addReference($referenceName . $count, $object);
        $count++;

        $object = new SampleB();
        $object->setSampleName('Ava');
        $object->setSampleBoolean(false);
        $object->setSampleInteger(-16);
        $object->setSampleString('Efgh, Ijk!');
        $object->setSampleText('negative sixteen dollars and sixteen cents');
        $object->setSampleDatetime(new \DateTime('2013-12-11T10:09:16'));
        $object->setSampleDate(new \DateTime('2012-11-02'));
        $object->setSampleTime(new \DateTime('03:02:01'));
        $object->setSampleDecimal(-16.16);
        $manager->persist($object);
        $manager->flush($object);
        $this->addReference($referenceName . $count, $object);
        $count++;

        $object = new SampleB();
        $object->setSampleName('Mason');
        $object->setSampleBoolean(false);
        $object->setSampleInteger(-32);
        $object->setSampleString('Lmno, Pqr!');
        $object->setSampleText('negative thirty-two dollars and thirty-two cents');
        $object->setSampleDatetime(new \DateTime('2013-12-11T10:09:17'));
        $object->setSampleDate(new \DateTime('2012-11-01'));
        $object->setSampleTime(new \DateTime('02:01:00'));
        $object->setSampleDecimal(-32.32);
        $manager->persist($object);
        $manager->flush($object);
        $this->addReference($referenceName . $count, $object);
        $count++;

        $object = new SampleB();
        $object->setSampleName('Lily');
        $object->setSampleBoolean(true);
        $object->setSampleInteger(-65);
        $object->setSampleString('Stuv, Wxy!');
        $object->setSampleText('negative sixty-four dollars and sixty-four cents');
        $object->setSampleDatetime(new \DateTime('2013-12-11T10:09:18'));
        $object->setSampleDate(new \DateTime('2012-10-31'));
        $object->setSampleTime(new \DateTime('01:00:59'));
        $object->setSampleDecimal(-64.64);
        $manager->persist($object);
        $manager->flush($object);
        $this->addReference($referenceName . $count, $object);
        $count++;

        $object = new SampleB();
        $object->setSampleName('Noah');
        $object->setSampleBoolean(true);
        $object->setSampleInteger(-129);
        $object->setSampleString('Zabc, Def!');
        $object->setSampleText('negative one hundred twenty-nine dollars and twenty-eight cents');
        $object->setSampleDatetime(new \DateTime('2013-12-11T10:09:19'));
        $object->setSampleDate(new \DateTime('2012-10-30'));
        $object->setSampleTime(new \DateTime('00:59:58'));
        $object->setSampleDecimal(-129.28);
        $manager->persist($object);
        $manager->flush($object);
        $this->addReference($referenceName . $count, $object);
        $count++;

        $object = new SampleB();
        $object->setSampleName('Zoe');
        $object->setSampleBoolean(true);
        $object->setSampleInteger(-259);
        $object->setSampleString('Ghij, Klm!');
        $object->setSampleText('negative two hundred fifty-eight dollars and fifty-six cents');
        $object->setSampleDatetime(new \DateTime('2013-12-11T10:09:20'));
        $object->setSampleDate(new \DateTime('2012-10-29'));
        $object->setSampleTime(new \DateTime('23:58:57'));
        $object->setSampleDecimal(-258.56);
        $manager->persist($object);
        $manager->flush($object);
        $this->addReference($referenceName . $count, $object);
        $count++;

        $object = new SampleB();
        $object->setSampleName('Lucas');
        $object->setSampleBoolean(false);
        $object->setSampleInteger(-517);
        $object->setSampleString('Nopq, Rst!');
        $object->setSampleText('negative five hundred seventeen dollars and twelve cents');
        $object->setSampleDatetime(new \DateTime('2013-12-11T10:09:21'));
        $object->setSampleDate(new \DateTime('2012-10-28'));
        $object->setSampleTime(new \DateTime('22:57:56'));
        $object->setSampleDecimal(-517.12);
        $manager->persist($object);
        $manager->flush($object);
        $this->addReference($referenceName . $count, $object);
        $count++;

        $object = new SampleB();
        $object->setSampleName('Chloe');
        $object->setSampleBoolean(false);
        $object->setSampleInteger(-1034);
        $object->setSampleString('Uvwx, Yza!');
        $object->setSampleText('negative one thousand thirty-four dollars and twenty-four cents');
        $object->setSampleDatetime(new \DateTime('2013-12-11T10:09:22'));
        $object->setSampleDate(new \DateTime('2012-10-27'));
        $object->setSampleTime(new \DateTime('21:56:55'));
        $object->setSampleDecimal(-1034.24);
        $manager->persist($object);
        $manager->flush($object);
        $this->addReference($referenceName . $count, $object);
        $count++;

        $object = new SampleB();
        $object->setSampleName('Jacob');
        $object->setSampleBoolean(null);
        $object->setSampleInteger(-2068);
        $object->setSampleString('Bcde, Fgh!');
        $object->setSampleText('negative two thousand sixty-eight dollars and forty-eight cents');
        $object->setSampleDatetime(new \DateTime('2013-12-11T10:09:23'));
        $object->setSampleDate(new \DateTime('2012-10-26'));
        $object->setSampleTime(new \DateTime('20:55:54'));
        $object->setSampleDecimal(-2068.48);
        $manager->persist($object);
        $manager->flush($object);
        $this->addReference($referenceName . $count, $object);
        $count++;
    }
}