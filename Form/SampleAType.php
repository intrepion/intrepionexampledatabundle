<?php

namespace Intrepion\Example\DataBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SampleAType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('sampleBoolean')
            ->add('sampleInteger')
            ->add('sampleString')
            ->add('sampleText')
            ->add('sampleDatetime')
            ->add('sampleDate')
            ->add('sampleTime')
            ->add('sampleDecimal')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Intrepion\Example\DataBundle\Entity\SampleA'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'intrepion_example_databundle_samplea';
    }
}
