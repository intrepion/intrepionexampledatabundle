<?php

namespace Intrepion\Example\DataBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SampleC
 */
class SampleC
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $sampleName;

    /**
     * @var boolean
     */
    private $sampleBoolean;

    /**
     * @var integer
     */
    private $sampleInteger;

    /**
     * @var string
     */
    private $sampleString;

    /**
     * @var string
     */
    private $sampleText;

    /**
     * @var \DateTime
     */
    private $sampleDatetime;

    /**
     * @var \DateTime
     */
    private $sampleDate;

    /**
     * @var \DateTime
     */
    private $sampleTime;

    /**
     * @var float
     */
    private $sampleDecimal;

    /**
     * @var \Intrepion\Example\DataBundle\Entity\SampleD
     */
    private $oneToOneD;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $oneToManyD;

    /**
     * @var \Intrepion\Example\DataBundle\Entity\SampleD
     */
    private $manyToOneD;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $manyToManyD;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->oneToManyD = new \Doctrine\Common\Collections\ArrayCollection();
        $this->manyToManyD = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sampleName
     *
     * @param string $sampleName
     * @return SampleC
     */
    public function setSampleName($sampleName)
    {
        $this->sampleName = $sampleName;
    
        return $this;
    }

    /**
     * Get sampleName
     *
     * @return string 
     */
    public function getSampleName()
    {
        return $this->sampleName;
    }

    /**
     * Set sampleBoolean
     *
     * @param boolean $sampleBoolean
     * @return SampleC
     */
    public function setSampleBoolean($sampleBoolean)
    {
        $this->sampleBoolean = $sampleBoolean;
    
        return $this;
    }

    /**
     * Get sampleBoolean
     *
     * @return boolean 
     */
    public function getSampleBoolean()
    {
        return $this->sampleBoolean;
    }

    /**
     * Set sampleInteger
     *
     * @param integer $sampleInteger
     * @return SampleC
     */
    public function setSampleInteger($sampleInteger)
    {
        $this->sampleInteger = $sampleInteger;
    
        return $this;
    }

    /**
     * Get sampleInteger
     *
     * @return integer 
     */
    public function getSampleInteger()
    {
        return $this->sampleInteger;
    }

    /**
     * Set sampleString
     *
     * @param string $sampleString
     * @return SampleC
     */
    public function setSampleString($sampleString)
    {
        $this->sampleString = $sampleString;
    
        return $this;
    }

    /**
     * Get sampleString
     *
     * @return string 
     */
    public function getSampleString()
    {
        return $this->sampleString;
    }

    /**
     * Set sampleText
     *
     * @param string $sampleText
     * @return SampleC
     */
    public function setSampleText($sampleText)
    {
        $this->sampleText = $sampleText;
    
        return $this;
    }

    /**
     * Get sampleText
     *
     * @return string 
     */
    public function getSampleText()
    {
        return $this->sampleText;
    }

    /**
     * Set sampleDatetime
     *
     * @param \DateTime $sampleDatetime
     * @return SampleC
     */
    public function setSampleDatetime($sampleDatetime)
    {
        $this->sampleDatetime = $sampleDatetime;
    
        return $this;
    }

    /**
     * Get sampleDatetime
     *
     * @return \DateTime 
     */
    public function getSampleDatetime()
    {
        return $this->sampleDatetime;
    }

    /**
     * Set sampleDate
     *
     * @param \DateTime $sampleDate
     * @return SampleC
     */
    public function setSampleDate($sampleDate)
    {
        $this->sampleDate = $sampleDate;
    
        return $this;
    }

    /**
     * Get sampleDate
     *
     * @return \DateTime 
     */
    public function getSampleDate()
    {
        return $this->sampleDate;
    }

    /**
     * Set sampleTime
     *
     * @param \DateTime $sampleTime
     * @return SampleC
     */
    public function setSampleTime($sampleTime)
    {
        $this->sampleTime = $sampleTime;
    
        return $this;
    }

    /**
     * Get sampleTime
     *
     * @return \DateTime 
     */
    public function getSampleTime()
    {
        return $this->sampleTime;
    }

    /**
     * Set sampleDecimal
     *
     * @param float $sampleDecimal
     * @return SampleC
     */
    public function setSampleDecimal($sampleDecimal)
    {
        $this->sampleDecimal = $sampleDecimal;
    
        return $this;
    }

    /**
     * Get sampleDecimal
     *
     * @return float 
     */
    public function getSampleDecimal()
    {
        return $this->sampleDecimal;
    }

    /**
     * Set oneToOneD
     *
     * @param \Intrepion\Example\DataBundle\Entity\SampleD $oneToOneD
     * @return SampleC
     */
    public function setOneToOneD(\Intrepion\Example\DataBundle\Entity\SampleD $oneToOneD = null)
    {
        $this->oneToOneD = $oneToOneD;
    
        return $this;
    }

    /**
     * Get oneToOneD
     *
     * @return \Intrepion\Example\DataBundle\Entity\SampleD 
     */
    public function getOneToOneD()
    {
        return $this->oneToOneD;
    }

    /**
     * Add oneToManyD
     *
     * @param \Intrepion\Example\DataBundle\Entity\SampleD $oneToManyD
     * @return SampleC
     */
    public function addOneToManyD(\Intrepion\Example\DataBundle\Entity\SampleD $oneToManyD)
    {
        $this->oneToManyD[] = $oneToManyD;
    
        return $this;
    }

    /**
     * Remove oneToManyD
     *
     * @param \Intrepion\Example\DataBundle\Entity\SampleD $oneToManyD
     */
    public function removeOneToManyD(\Intrepion\Example\DataBundle\Entity\SampleD $oneToManyD)
    {
        $this->oneToManyD->removeElement($oneToManyD);
    }

    /**
     * Get oneToManyD
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getOneToManyD()
    {
        return $this->oneToManyD;
    }

    /**
     * Set manyToOneD
     *
     * @param \Intrepion\Example\DataBundle\Entity\SampleD $manyToOneD
     * @return SampleC
     */
    public function setManyToOneD(\Intrepion\Example\DataBundle\Entity\SampleD $manyToOneD = null)
    {
        $this->manyToOneD = $manyToOneD;
    
        return $this;
    }

    /**
     * Get manyToOneD
     *
     * @return \Intrepion\Example\DataBundle\Entity\SampleD 
     */
    public function getManyToOneD()
    {
        return $this->manyToOneD;
    }

    /**
     * Add manyToManyD
     *
     * @param \Intrepion\Example\DataBundle\Entity\SampleD $manyToManyD
     * @return SampleC
     */
    public function addManyToManyD(\Intrepion\Example\DataBundle\Entity\SampleD $manyToManyD)
    {
        $this->manyToManyD[] = $manyToManyD;
    
        return $this;
    }

    /**
     * Remove manyToManyD
     *
     * @param \Intrepion\Example\DataBundle\Entity\SampleD $manyToManyD
     */
    public function removeManyToManyD(\Intrepion\Example\DataBundle\Entity\SampleD $manyToManyD)
    {
        $this->manyToManyD->removeElement($manyToManyD);
    }

    /**
     * Get manyToManyD
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getManyToManyD()
    {
        return $this->manyToManyD;
    }
}
