<?php

namespace Intrepion\Example\DataBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SampleA
 */
class SampleA
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $sampleName;

    /**
     * @var boolean
     */
    private $sampleBoolean;

    /**
     * @var integer
     */
    private $sampleInteger;

    /**
     * @var string
     */
    private $sampleString;

    /**
     * @var string
     */
    private $sampleText;

    /**
     * @var \DateTime
     */
    private $sampleDatetime;

    /**
     * @var \DateTime
     */
    private $sampleDate;

    /**
     * @var \DateTime
     */
    private $sampleTime;

    /**
     * @var float
     */
    private $sampleDecimal;

    /**
     * @var \Intrepion\Example\DataBundle\Entity\SampleB
     */
    private $oneToOneB;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $oneToManyB;

    /**
     * @var \Intrepion\Example\DataBundle\Entity\SampleB
     */
    private $manyToOneB;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $manyToManyB;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->oneToManyB = new \Doctrine\Common\Collections\ArrayCollection();
        $this->manyToManyB = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sampleName
     *
     * @param string $sampleName
     * @return SampleA
     */
    public function setSampleName($sampleName)
    {
        $this->sampleName = $sampleName;
    
        return $this;
    }

    /**
     * Get sampleName
     *
     * @return string 
     */
    public function getSampleName()
    {
        return $this->sampleName;
    }

    /**
     * Set sampleBoolean
     *
     * @param boolean $sampleBoolean
     * @return SampleA
     */
    public function setSampleBoolean($sampleBoolean)
    {
        $this->sampleBoolean = $sampleBoolean;
    
        return $this;
    }

    /**
     * Get sampleBoolean
     *
     * @return boolean 
     */
    public function getSampleBoolean()
    {
        return $this->sampleBoolean;
    }

    /**
     * Set sampleInteger
     *
     * @param integer $sampleInteger
     * @return SampleA
     */
    public function setSampleInteger($sampleInteger)
    {
        $this->sampleInteger = $sampleInteger;
    
        return $this;
    }

    /**
     * Get sampleInteger
     *
     * @return integer 
     */
    public function getSampleInteger()
    {
        return $this->sampleInteger;
    }

    /**
     * Set sampleString
     *
     * @param string $sampleString
     * @return SampleA
     */
    public function setSampleString($sampleString)
    {
        $this->sampleString = $sampleString;
    
        return $this;
    }

    /**
     * Get sampleString
     *
     * @return string 
     */
    public function getSampleString()
    {
        return $this->sampleString;
    }

    /**
     * Set sampleText
     *
     * @param string $sampleText
     * @return SampleA
     */
    public function setSampleText($sampleText)
    {
        $this->sampleText = $sampleText;
    
        return $this;
    }

    /**
     * Get sampleText
     *
     * @return string 
     */
    public function getSampleText()
    {
        return $this->sampleText;
    }

    /**
     * Set sampleDatetime
     *
     * @param \DateTime $sampleDatetime
     * @return SampleA
     */
    public function setSampleDatetime($sampleDatetime)
    {
        $this->sampleDatetime = $sampleDatetime;
    
        return $this;
    }

    /**
     * Get sampleDatetime
     *
     * @return \DateTime 
     */
    public function getSampleDatetime()
    {
        return $this->sampleDatetime;
    }

    /**
     * Set sampleDate
     *
     * @param \DateTime $sampleDate
     * @return SampleA
     */
    public function setSampleDate($sampleDate)
    {
        $this->sampleDate = $sampleDate;
    
        return $this;
    }

    /**
     * Get sampleDate
     *
     * @return \DateTime 
     */
    public function getSampleDate()
    {
        return $this->sampleDate;
    }

    /**
     * Set sampleTime
     *
     * @param \DateTime $sampleTime
     * @return SampleA
     */
    public function setSampleTime($sampleTime)
    {
        $this->sampleTime = $sampleTime;
    
        return $this;
    }

    /**
     * Get sampleTime
     *
     * @return \DateTime 
     */
    public function getSampleTime()
    {
        return $this->sampleTime;
    }

    /**
     * Set sampleDecimal
     *
     * @param float $sampleDecimal
     * @return SampleA
     */
    public function setSampleDecimal($sampleDecimal)
    {
        $this->sampleDecimal = $sampleDecimal;
    
        return $this;
    }

    /**
     * Get sampleDecimal
     *
     * @return float 
     */
    public function getSampleDecimal()
    {
        return $this->sampleDecimal;
    }

    /**
     * Set oneToOneB
     *
     * @param \Intrepion\Example\DataBundle\Entity\SampleB $oneToOneB
     * @return SampleA
     */
    public function setOneToOneB(\Intrepion\Example\DataBundle\Entity\SampleB $oneToOneB = null)
    {
        $this->oneToOneB = $oneToOneB;
    
        return $this;
    }

    /**
     * Get oneToOneB
     *
     * @return \Intrepion\Example\DataBundle\Entity\SampleB 
     */
    public function getOneToOneB()
    {
        return $this->oneToOneB;
    }

    /**
     * Add oneToManyB
     *
     * @param \Intrepion\Example\DataBundle\Entity\SampleB $oneToManyB
     * @return SampleA
     */
    public function addOneToManyB(\Intrepion\Example\DataBundle\Entity\SampleB $oneToManyB)
    {
        $this->oneToManyB[] = $oneToManyB;
    
        return $this;
    }

    /**
     * Remove oneToManyB
     *
     * @param \Intrepion\Example\DataBundle\Entity\SampleB $oneToManyB
     */
    public function removeOneToManyB(\Intrepion\Example\DataBundle\Entity\SampleB $oneToManyB)
    {
        $this->oneToManyB->removeElement($oneToManyB);
    }

    /**
     * Get oneToManyB
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getOneToManyB()
    {
        return $this->oneToManyB;
    }

    /**
     * Set manyToOneB
     *
     * @param \Intrepion\Example\DataBundle\Entity\SampleB $manyToOneB
     * @return SampleA
     */
    public function setManyToOneB(\Intrepion\Example\DataBundle\Entity\SampleB $manyToOneB = null)
    {
        $this->manyToOneB = $manyToOneB;
    
        return $this;
    }

    /**
     * Get manyToOneB
     *
     * @return \Intrepion\Example\DataBundle\Entity\SampleB 
     */
    public function getManyToOneB()
    {
        return $this->manyToOneB;
    }

    /**
     * Add manyToManyB
     *
     * @param \Intrepion\Example\DataBundle\Entity\SampleB $manyToManyB
     * @return SampleA
     */
    public function addManyToManyB(\Intrepion\Example\DataBundle\Entity\SampleB $manyToManyB)
    {
        $this->manyToManyB[] = $manyToManyB;
    
        return $this;
    }

    /**
     * Remove manyToManyB
     *
     * @param \Intrepion\Example\DataBundle\Entity\SampleB $manyToManyB
     */
    public function removeManyToManyB(\Intrepion\Example\DataBundle\Entity\SampleB $manyToManyB)
    {
        $this->manyToManyB->removeElement($manyToManyB);
    }

    /**
     * Get manyToManyB
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getManyToManyB()
    {
        return $this->manyToManyB;
    }
}