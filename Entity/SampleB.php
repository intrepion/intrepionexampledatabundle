<?php

namespace Intrepion\Example\DataBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SampleB
 */
class SampleB
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $sampleName;

    /**
     * @var boolean
     */
    private $sampleBoolean;

    /**
     * @var integer
     */
    private $sampleInteger;

    /**
     * @var string
     */
    private $sampleString;

    /**
     * @var string
     */
    private $sampleText;

    /**
     * @var \DateTime
     */
    private $sampleDatetime;

    /**
     * @var \DateTime
     */
    private $sampleDate;

    /**
     * @var \DateTime
     */
    private $sampleTime;

    /**
     * @var float
     */
    private $sampleDecimal;

    /**
     * @var \Intrepion\Example\DataBundle\Entity\SampleA
     */
    private $oneToOneA;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $oneToManyA;

    /**
     * @var \Intrepion\Example\DataBundle\Entity\SampleA
     */
    private $manyToOneA;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $manyToManyA;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->oneToManyA = new \Doctrine\Common\Collections\ArrayCollection();
        $this->manyToManyA = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sampleName
     *
     * @param string $sampleName
     * @return SampleB
     */
    public function setSampleName($sampleName)
    {
        $this->sampleName = $sampleName;
    
        return $this;
    }

    /**
     * Get sampleName
     *
     * @return string 
     */
    public function getSampleName()
    {
        return $this->sampleName;
    }

    /**
     * Set sampleBoolean
     *
     * @param boolean $sampleBoolean
     * @return SampleB
     */
    public function setSampleBoolean($sampleBoolean)
    {
        $this->sampleBoolean = $sampleBoolean;
    
        return $this;
    }

    /**
     * Get sampleBoolean
     *
     * @return boolean 
     */
    public function getSampleBoolean()
    {
        return $this->sampleBoolean;
    }

    /**
     * Set sampleInteger
     *
     * @param integer $sampleInteger
     * @return SampleB
     */
    public function setSampleInteger($sampleInteger)
    {
        $this->sampleInteger = $sampleInteger;
    
        return $this;
    }

    /**
     * Get sampleInteger
     *
     * @return integer 
     */
    public function getSampleInteger()
    {
        return $this->sampleInteger;
    }

    /**
     * Set sampleString
     *
     * @param string $sampleString
     * @return SampleB
     */
    public function setSampleString($sampleString)
    {
        $this->sampleString = $sampleString;
    
        return $this;
    }

    /**
     * Get sampleString
     *
     * @return string 
     */
    public function getSampleString()
    {
        return $this->sampleString;
    }

    /**
     * Set sampleText
     *
     * @param string $sampleText
     * @return SampleB
     */
    public function setSampleText($sampleText)
    {
        $this->sampleText = $sampleText;
    
        return $this;
    }

    /**
     * Get sampleText
     *
     * @return string 
     */
    public function getSampleText()
    {
        return $this->sampleText;
    }

    /**
     * Set sampleDatetime
     *
     * @param \DateTime $sampleDatetime
     * @return SampleB
     */
    public function setSampleDatetime($sampleDatetime)
    {
        $this->sampleDatetime = $sampleDatetime;
    
        return $this;
    }

    /**
     * Get sampleDatetime
     *
     * @return \DateTime 
     */
    public function getSampleDatetime()
    {
        return $this->sampleDatetime;
    }

    /**
     * Set sampleDate
     *
     * @param \DateTime $sampleDate
     * @return SampleB
     */
    public function setSampleDate($sampleDate)
    {
        $this->sampleDate = $sampleDate;
    
        return $this;
    }

    /**
     * Get sampleDate
     *
     * @return \DateTime 
     */
    public function getSampleDate()
    {
        return $this->sampleDate;
    }

    /**
     * Set sampleTime
     *
     * @param \DateTime $sampleTime
     * @return SampleB
     */
    public function setSampleTime($sampleTime)
    {
        $this->sampleTime = $sampleTime;
    
        return $this;
    }

    /**
     * Get sampleTime
     *
     * @return \DateTime 
     */
    public function getSampleTime()
    {
        return $this->sampleTime;
    }

    /**
     * Set sampleDecimal
     *
     * @param float $sampleDecimal
     * @return SampleB
     */
    public function setSampleDecimal($sampleDecimal)
    {
        $this->sampleDecimal = $sampleDecimal;
    
        return $this;
    }

    /**
     * Get sampleDecimal
     *
     * @return float 
     */
    public function getSampleDecimal()
    {
        return $this->sampleDecimal;
    }

    /**
     * Set oneToOneA
     *
     * @param \Intrepion\Example\DataBundle\Entity\SampleA $oneToOneA
     * @return SampleB
     */
    public function setOneToOneA(\Intrepion\Example\DataBundle\Entity\SampleA $oneToOneA = null)
    {
        $this->oneToOneA = $oneToOneA;
    
        return $this;
    }

    /**
     * Get oneToOneA
     *
     * @return \Intrepion\Example\DataBundle\Entity\SampleA 
     */
    public function getOneToOneA()
    {
        return $this->oneToOneA;
    }

    /**
     * Add oneToManyA
     *
     * @param \Intrepion\Example\DataBundle\Entity\SampleA $oneToManyA
     * @return SampleB
     */
    public function addOneToManyA(\Intrepion\Example\DataBundle\Entity\SampleA $oneToManyA)
    {
        $this->oneToManyA[] = $oneToManyA;
    
        return $this;
    }

    /**
     * Remove oneToManyA
     *
     * @param \Intrepion\Example\DataBundle\Entity\SampleA $oneToManyA
     */
    public function removeOneToManyA(\Intrepion\Example\DataBundle\Entity\SampleA $oneToManyA)
    {
        $this->oneToManyA->removeElement($oneToManyA);
    }

    /**
     * Get oneToManyA
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getOneToManyA()
    {
        return $this->oneToManyA;
    }

    /**
     * Set manyToOneA
     *
     * @param \Intrepion\Example\DataBundle\Entity\SampleA $manyToOneA
     * @return SampleB
     */
    public function setManyToOneA(\Intrepion\Example\DataBundle\Entity\SampleA $manyToOneA = null)
    {
        $this->manyToOneA = $manyToOneA;
    
        return $this;
    }

    /**
     * Get manyToOneA
     *
     * @return \Intrepion\Example\DataBundle\Entity\SampleA 
     */
    public function getManyToOneA()
    {
        return $this->manyToOneA;
    }

    /**
     * Add manyToManyA
     *
     * @param \Intrepion\Example\DataBundle\Entity\SampleA $manyToManyA
     * @return SampleB
     */
    public function addManyToManyA(\Intrepion\Example\DataBundle\Entity\SampleA $manyToManyA)
    {
        $this->manyToManyA[] = $manyToManyA;
    
        return $this;
    }

    /**
     * Remove manyToManyA
     *
     * @param \Intrepion\Example\DataBundle\Entity\SampleA $manyToManyA
     */
    public function removeManyToManyA(\Intrepion\Example\DataBundle\Entity\SampleA $manyToManyA)
    {
        $this->manyToManyA->removeElement($manyToManyA);
    }

    /**
     * Get manyToManyA
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getManyToManyA()
    {
        return $this->manyToManyA;
    }
}