<?php

namespace Intrepion\Example\DataBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SampleD
 */
class SampleD
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $sampleName;

    /**
     * @var boolean
     */
    private $sampleBoolean;

    /**
     * @var integer
     */
    private $sampleInteger;

    /**
     * @var string
     */
    private $sampleString;

    /**
     * @var string
     */
    private $sampleText;

    /**
     * @var \DateTime
     */
    private $sampleDatetime;

    /**
     * @var \DateTime
     */
    private $sampleDate;

    /**
     * @var \DateTime
     */
    private $sampleTime;

    /**
     * @var float
     */
    private $sampleDecimal;

    /**
     * @var \Intrepion\Example\DataBundle\Entity\SampleC
     */
    private $oneToOneC;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $oneToManyC;

    /**
     * @var \Intrepion\Example\DataBundle\Entity\SampleC
     */
    private $manyToOneC;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $manyToManyC;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->oneToManyC = new \Doctrine\Common\Collections\ArrayCollection();
        $this->manyToManyC = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sampleName
     *
     * @param string $sampleName
     * @return SampleD
     */
    public function setSampleName($sampleName)
    {
        $this->sampleName = $sampleName;
    
        return $this;
    }

    /**
     * Get sampleName
     *
     * @return string 
     */
    public function getSampleName()
    {
        return $this->sampleName;
    }

    /**
     * Set sampleBoolean
     *
     * @param boolean $sampleBoolean
     * @return SampleD
     */
    public function setSampleBoolean($sampleBoolean)
    {
        $this->sampleBoolean = $sampleBoolean;
    
        return $this;
    }

    /**
     * Get sampleBoolean
     *
     * @return boolean 
     */
    public function getSampleBoolean()
    {
        return $this->sampleBoolean;
    }

    /**
     * Set sampleInteger
     *
     * @param integer $sampleInteger
     * @return SampleD
     */
    public function setSampleInteger($sampleInteger)
    {
        $this->sampleInteger = $sampleInteger;
    
        return $this;
    }

    /**
     * Get sampleInteger
     *
     * @return integer 
     */
    public function getSampleInteger()
    {
        return $this->sampleInteger;
    }

    /**
     * Set sampleString
     *
     * @param string $sampleString
     * @return SampleD
     */
    public function setSampleString($sampleString)
    {
        $this->sampleString = $sampleString;
    
        return $this;
    }

    /**
     * Get sampleString
     *
     * @return string 
     */
    public function getSampleString()
    {
        return $this->sampleString;
    }

    /**
     * Set sampleText
     *
     * @param string $sampleText
     * @return SampleD
     */
    public function setSampleText($sampleText)
    {
        $this->sampleText = $sampleText;
    
        return $this;
    }

    /**
     * Get sampleText
     *
     * @return string 
     */
    public function getSampleText()
    {
        return $this->sampleText;
    }

    /**
     * Set sampleDatetime
     *
     * @param \DateTime $sampleDatetime
     * @return SampleD
     */
    public function setSampleDatetime($sampleDatetime)
    {
        $this->sampleDatetime = $sampleDatetime;
    
        return $this;
    }

    /**
     * Get sampleDatetime
     *
     * @return \DateTime 
     */
    public function getSampleDatetime()
    {
        return $this->sampleDatetime;
    }

    /**
     * Set sampleDate
     *
     * @param \DateTime $sampleDate
     * @return SampleD
     */
    public function setSampleDate($sampleDate)
    {
        $this->sampleDate = $sampleDate;
    
        return $this;
    }

    /**
     * Get sampleDate
     *
     * @return \DateTime 
     */
    public function getSampleDate()
    {
        return $this->sampleDate;
    }

    /**
     * Set sampleTime
     *
     * @param \DateTime $sampleTime
     * @return SampleD
     */
    public function setSampleTime($sampleTime)
    {
        $this->sampleTime = $sampleTime;
    
        return $this;
    }

    /**
     * Get sampleTime
     *
     * @return \DateTime 
     */
    public function getSampleTime()
    {
        return $this->sampleTime;
    }

    /**
     * Set sampleDecimal
     *
     * @param float $sampleDecimal
     * @return SampleD
     */
    public function setSampleDecimal($sampleDecimal)
    {
        $this->sampleDecimal = $sampleDecimal;
    
        return $this;
    }

    /**
     * Get sampleDecimal
     *
     * @return float 
     */
    public function getSampleDecimal()
    {
        return $this->sampleDecimal;
    }

    /**
     * Set oneToOneC
     *
     * @param \Intrepion\Example\DataBundle\Entity\SampleC $oneToOneC
     * @return SampleD
     */
    public function setOneToOneC(\Intrepion\Example\DataBundle\Entity\SampleC $oneToOneC = null)
    {
        $this->oneToOneC = $oneToOneC;
    
        return $this;
    }

    /**
     * Get oneToOneC
     *
     * @return \Intrepion\Example\DataBundle\Entity\SampleC 
     */
    public function getOneToOneC()
    {
        return $this->oneToOneC;
    }

    /**
     * Add oneToManyC
     *
     * @param \Intrepion\Example\DataBundle\Entity\SampleC $oneToManyC
     * @return SampleD
     */
    public function addOneToManyC(\Intrepion\Example\DataBundle\Entity\SampleC $oneToManyC)
    {
        $this->oneToManyC[] = $oneToManyC;
    
        return $this;
    }

    /**
     * Remove oneToManyC
     *
     * @param \Intrepion\Example\DataBundle\Entity\SampleC $oneToManyC
     */
    public function removeOneToManyC(\Intrepion\Example\DataBundle\Entity\SampleC $oneToManyC)
    {
        $this->oneToManyC->removeElement($oneToManyC);
    }

    /**
     * Get oneToManyC
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getOneToManyC()
    {
        return $this->oneToManyC;
    }

    /**
     * Set manyToOneC
     *
     * @param \Intrepion\Example\DataBundle\Entity\SampleC $manyToOneC
     * @return SampleD
     */
    public function setManyToOneC(\Intrepion\Example\DataBundle\Entity\SampleC $manyToOneC = null)
    {
        $this->manyToOneC = $manyToOneC;
    
        return $this;
    }

    /**
     * Get manyToOneC
     *
     * @return \Intrepion\Example\DataBundle\Entity\SampleC 
     */
    public function getManyToOneC()
    {
        return $this->manyToOneC;
    }

    /**
     * Add manyToManyC
     *
     * @param \Intrepion\Example\DataBundle\Entity\SampleC $manyToManyC
     * @return SampleD
     */
    public function addManyToManyC(\Intrepion\Example\DataBundle\Entity\SampleC $manyToManyC)
    {
        $this->manyToManyC[] = $manyToManyC;
    
        return $this;
    }

    /**
     * Remove manyToManyC
     *
     * @param \Intrepion\Example\DataBundle\Entity\SampleC $manyToManyC
     */
    public function removeManyToManyC(\Intrepion\Example\DataBundle\Entity\SampleC $manyToManyC)
    {
        $this->manyToManyC->removeElement($manyToManyC);
    }

    /**
     * Get manyToManyC
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getManyToManyC()
    {
        return $this->manyToManyC;
    }
}
