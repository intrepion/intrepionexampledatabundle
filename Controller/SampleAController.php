<?php

namespace Intrepion\Example\DataBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Intrepion\Example\DataBundle\Entity\SampleA;
use Intrepion\Example\DataBundle\Form\SampleAType;

/**
 * SampleA controller.
 *
 */
class SampleAController extends Controller
{

    /**
     * Lists all SampleA entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('IntrepionExampleDataBundle:SampleA')->findAll();

        return $this->render('IntrepionExampleDataBundle:SampleA:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new SampleA entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new SampleA();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('samplea_show', array('id' => $entity->getId())));
        }

        return $this->render('IntrepionExampleDataBundle:SampleA:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a SampleA entity.
    *
    * @param SampleA $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(SampleA $entity)
    {
        $form = $this->createForm(new SampleAType(), $entity, array(
            'action' => $this->generateUrl('samplea_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new SampleA entity.
     *
     */
    public function newAction()
    {
        $entity = new SampleA();
        $form   = $this->createCreateForm($entity);

        return $this->render('IntrepionExampleDataBundle:SampleA:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a SampleA entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('IntrepionExampleDataBundle:SampleA')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find SampleA entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('IntrepionExampleDataBundle:SampleA:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing SampleA entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('IntrepionExampleDataBundle:SampleA')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find SampleA entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('IntrepionExampleDataBundle:SampleA:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a SampleA entity.
    *
    * @param SampleA $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(SampleA $entity)
    {
        $form = $this->createForm(new SampleAType(), $entity, array(
            'action' => $this->generateUrl('samplea_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing SampleA entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('IntrepionExampleDataBundle:SampleA')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find SampleA entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('samplea_edit', array('id' => $id)));
        }

        return $this->render('IntrepionExampleDataBundle:SampleA:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a SampleA entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('IntrepionExampleDataBundle:SampleA')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find SampleA entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('samplea'));
    }

    /**
     * Creates a form to delete a SampleA entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('samplea_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
